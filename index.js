'use strict';

var format = require('util').format;
var sjcl = require('sjcl');
var bitcore = require('bitcore-lib');
var coininfo = require('coininfo');
var request = require('xhr-request');

var HOST = 'https://mnsig.com/server';
var Headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
};
var SPLIT = {
  min: 0.15 * 1e8,  // Split if at least 0.15 BTC is available for outputs
  num: 5            // Split into 5 outputs
};


function LocalError(message, code) {
  this.message = message;
  this.statusMessage = this.message;
  this.statusCode = code;
  this.stack = (new Error()).stack;
  return this;
}
LocalError.prototype = Object.create(Error.prototype);
LocalError.prototype.name = 'LocalError';


/**
 * Register other networks.
 */
function registerAlt() {
    var dashmain = coininfo.dash.main.toBitcore();
    dashmain.name = 'dash';
    dashmain.alias = 'dashmainnet';
    bitcore.Networks.add(dashmain);

    var dashtest = coininfo.dash.test.toBitcore();
    dashtest.pubkeyhash = 140;
    dashtest.name = 'dashtest';
    dashtest.alias = 'dashtestnet';
    bitcore.Networks.add(dashtest);
}
registerAlt();


/**
 * @param plainPassword   Plain text password to be used for encrypting the private key
 * @param derivation      Derivation method, null or 'bip44'
 * @param network         'bitcoin' for the bitcoin mainnet or 'testnet' for bitcoin testnet
 * @returns object containing 'xpub' to be used my mnsig and 'xprivEnc'
 */
function generateKey(plainPassword, derivation, network) {
  var net = null;
  switch (network) {
    case 'bitcoin':
      net = bitcore.Networks.livenet;
      break;
    case 'testnet':
      net = bitcore.Networks.testnet;
      break;
    case 'dash':
      net = bitcore.Networks.get('dash');
      break;
    case 'dashtest':
      net = bitcore.Networks.get('dashtest');
      break;
  }
  if (!net) {
      throw new LocalError('unknown network', -400);
  }

  var master = new bitcore.HDPrivateKey(net);
  var enc = sjcl.encrypt(plainPassword, master.toString());
  var derived, xpub, enc;

  if (derivation.toLowerCase() === 'bip44') {
    derived = master.derive("m/44'/0'/0'");
    xpub = derived.hdPublicKey;
  } else {
    xpub = master.hdPublicKey;
  }

  return {
    'xpub': xpub.toString(),
    'xprivEnc': enc
  };
}


/**
 * @param plainPassword   Plain text password to be used for decrypting the private key
 * @param derivation      Derivation method, null or 'bip44'
 * @returns a xpriv ready to be used by mnsig for locally signing
 */
function decryptKey(plainPassword, xprivEnc, derivation) {
  var xpriv, xprivRaw;

  try {
    xprivRaw = sjcl.decrypt(plainPassword, xprivEnc);
  } catch (e) {
    console.error(e);
    return null;
  }

  xpriv = new bitcore.HDPrivateKey(xprivRaw);
  if (derivation.toLowerCase() === 'bip44') {
    return xpriv.derive("m/44'/0'/0'");
  } else {
    return xpriv;
  }
}


/**
 * @param serverInstance  Server instance to use, string.
 * @param opts            Information about this client.
 * @param opts.token      Client token, string.
 * @param opts.host       Server host, string.
 * @param opts.split      UTXO split config.
 * @param opts.split.min  Minimum spare output amount to consider for splitting.
 * @param opts.split.num  Split spare output amount into num outputs.
 * @param opts.nosplit    Disable UTXO splitting.
 */
function Client(serverInstance, opts) {
  var options = opts || {};
  this.instance = serverInstance;
  this.host = options.host ? options.host : HOST;
  this.token = options.token;
  if (options.nosplit !== true) {
    this.split = options.split ? options.split : SPLIT;
  } else {
    this.split = null;
  }

  return this;
}


Client.prototype.request = function(cb, path, method, data) {
  var url = format('%s/%s/api/v1/%s', this.host, this.instance, path);
  var opts = {
    headers: this.token ? {'Token': this.token} : {},
    method: method || 'GET',
    timeout: 15 * 1000,
    json: true,
  };
  if (data) {
    if (opts.method !== 'GET') {
      opts.body = data;
    } else {
      opts.query = data;
    }
  }

  return request(url, opts, function(err, data, response) {
    if (err) {
      if (err.statusCode === undefined || err.statusCode === 0) {
        err.statusCode = -1;
        err.statusMessage = err.errno;
      }
    } else if (response.statusCode !== 200) {
      err = {};
      err.statusCode = response.statusCode;
      err.statusMessage = data.message;
      data = null;
    }

    setTimeout(function() {
      cb(err, data);
    });
  });
};


/**
 * @param cb              Callback function (err, json)
 */
Client.prototype.userdata = function(cb) {
  return this.request(cb, 'user/data', 'GET');
};


/**
 * Signup is typicall used only by web clients.
 *
 * @param opts            Information about the user that wants to sign up.
 * @param opts.username   The username as a string.
 * @param opts.hashed_pwd A non-plaintext version of the password.
 * @param cb              Callback function (err, json)
 */
Client.prototype.signup = function(opts, cb) {
  return this.request(cb, 'user/signup', 'POST', opts);
};


/**
 * Login is typically used only by web clients.
 *
 * @param opts            Information about the user that wants to log in.
 * @param opts.username   The username as a string.
 * @param opts.hashedpwd  A non-plaintext version of the password.
 * @param opts.totp       2FA totp code.
 * @param cb              Callback function (err, json)
 */
Client.prototype.login = function(opts, cb) {
  var opts = {
    username: opts.username,
    hashed_pwd: opts.hashedpwd,
    totp: opts.totp
  };
  return this.request(cb, 'user/login', 'POST', opts);
};


/**
 * Logout is typically used only by web clients.
 *
 * @param cb              Callback function (err, json)
 */
Client.prototype.logout = function(cb) {
  return this.request(cb, 'user/logout', 'GET');
};


/**
 * @param opts            Information about the wallet being created.
 * @param opts.wallet     Wallet name, string.
 * @param opts.type       Wallet type, 'bitcoin' (bitcoin mainnet) and
 *                          'testnet' (bitcoin testnet) supported.
 * @param opts.m          Signatures required, positive integer.
 * @param opts.n          Number of signers, n >= m.
 * @param opts.derivation Derivation method, null or 'bip44' (default).
 * @param cb              Callback function (err, json)
 */
Client.prototype.createWallet = function(opts, cb) {
  var opts = {
    wallet: opts.wallet,
    wallet_type: opts.type,
    wallet_derivation: opts.derivation === undefined ? 'bip44' : opts.derivation,
    m: opts.m, n: opts.n
  };
  return this.request(cb, 'wallet/', 'POST', opts);
};

/**
 * Add a key to an existing wallet.
 *
 * @param opts            Information about the key being added.
 * @param opts.wallet     Wallet name, string.
 * @param opts.xpub       xpub that will be used for deriving addresses
 *                          on the server, a string.
 * @param opts.encxpriv   Encrypted xpriv to be retrieved when signing,
 *                          optional.
 * @param cb              Callback function (err, json)
 */
Client.prototype.addWalletKey = function(opts, cb) {
  var opts = {
    wallet: opts.wallet,
    xpub: opts.xpub,
    encrypted_xpriv: opts.encxpriv
  };
  return this.request(cb, 'wallet/', 'PUT', opts);
};


/**
 * @param opts            Information about the wallet being retrieved.
 * @param opts.wallet     Wallet name, string.
 * @param cb              Callback function (err, json)
 */
Client.prototype.walletInfo = function(opts, cb) {
  return this.request(cb, format('wallet/%s', opts.wallet), 'GET');
};


/**
 * @param opts            Information about the wallet to retrieve keys.
 * @param opts.wallet     Wallet name, string.
 * @param cb              Callback function (err, json)
 */
Client.prototype.walletKeys = function(opts, cb) {
  return this.request(cb, format('wallet/%s/keys', opts.wallet), 'GET');
};


/**
 * @param opts            Information about the wallet to retrieve balance.
 * @param opts.wallet     Wallet name, string.
 * @param opts.target     Number of confirmations to consider for confirmed
 *                          balance, default 2.
 * @param cb              Callback function (err, json)
 */
Client.prototype.balance = function(opts, cb) {
  var params = null;
  if (opts.target) {
    params = {target: opts.target};
  }
  return this.request(cb, format('wallet/%s/balance', opts.wallet), 'GET', params);
};


/**
 * @param opts            Information about the wallet to derive an address.
 * @param opts.wallet     Wallet name, string.
 * @param opts.change     Derive an internal (change) address? Boolean.
 * @param cb              Callback function (err, json)
 */
Client.prototype.newAddress = function(opts, cb) {
  var params = {};
  if (opts.change !== undefined) {
    params.change = opts.change;
  };
  return this.request(cb, format('wallet/%s/address', opts.wallet), 'POST', params);
};


/**
 * @param opts            Information about the wallet to estimate fees.
 * @param opts.wallet     Wallet name, string.
 * @param opts.target     Block target, integer.
 * @param cb              Callback function (err, json)
 */
Client.prototype.estimateFee = function(opts, cb) {
  var target = 2;
  if (opts.target !== undefined) {
    target = opts.target;
  }
  return this.request(cb, format('wallet/%s/estimatefee/%d', opts.wallet, target), 'GET');
};


/**
 * Create a new API token.
 *
 * @param opts            Information about the wallet to add a token.
 * @param opts.wallet     Wallet name, string.
 * @param opts.label      Custom label for this token, a string
 * @param opts.maxAmount  Amount in BTC above which this token is disallowed
 *                          from sending signed proposals
 * @param opts.restrictIP Restrict access to this token to one or more IP
 *                          addresses or ranges, a list.
 *                          Example: ['8.8.4.4', '8.8.8.0/32']
 * @param cb              Callback function (err, json)
 */
Client.prototype.addToken = function(opts, cb) {
  var params = {
    restrict_to: opts.restrictIP.join(','),
    max_txamount: opts.maxAmount,
    label: opts.label
  };
  return this.request(cb, format('wallet/%s/token/', opts.wallet), 'POST', params);
};


/**
 * @param opts            Information about the wallet to delete a token.
 * @param opts.wallet     Wallet name, string.
 * @param opts.tokenId    Token id to disable.
 * @param cb              Callback function (err, json)
 */
Client.prototype.deleteToken = function(opts, cb) {
  return this.request(cb, format('wallet/%s/token/%s', opts.wallet, opts.tokenId), 'DELETE');
};


/**
 * @param opts            Information about the wallet to add an user.
 * @param opts.wallet     Wallet name, string.
 * @param opts.username   Username for the user to be added, string.
 * @param opts.perms      List of permissions, one or more of 'VIEW', 'DERIVE',
 *                          'SIGN'.
 * @param cb              Callback function (err, json)
 */
Client.prototype.addWalletUser = function(opts, cb) {
  var params = {
    username: opts.username,
    permissions: opts.perms.join(',')
  };
  return this.request(cb, format('wallet/%s/user', opts.wallet), 'POST', params);
};


/**
 * @param opts            Information about the wallet to retrieve transactions.
 * @param opts.wallet     Wallet name, string.
 * @param opts.export     Return transactions as CSV, boolean (default false).
 * @param opts.minTime    Return transactions with a complete date equal or
 *                          greater to this number, unix timestamp.
 * @param opts.maxTime    Return transactions with a complete date less than
 *                          this number, unix timestamp.
 * @param opts.page       Page bookmark, string.
 * @param cb              Callback function (err, json)
 */
Client.prototype.txlist = function(opts, cb) {
  var params = null;
  if (opts.export || opts.minTime || opts.maxTime || opts.page) {
    params = {};
    if (opts.export) params.export = 1;
    if (opts.minTime) params.min_ts = opts.minTime;
    if (opts.maxTime) params.max_ts = opts.maxTime;
    if (opts.page) params.page = opts.page;
  }
  return this.request(cb, format('wallet/%s/transactions', opts.wallet), 'GET', params);
};


/**
 * @param opts            Information about the wallet to retrieve one transaction.
 * @param opts.wallet     Wallet name, string.
 * @param opts.txid       Bitcoin transaction id.
 * @param opts.extid      External id associated to the transaction be returned.
 * @param cb              Callback function (err, json)
 */
Client.prototype.txget = function(opts, cb) {
  var params = null;
  if (opts.txid || opts.extid) {
    params = {};
    if (opts.txid) params.txid = opts.txid;
    else if (opts.extid) params.external_id = opts.extid;
  }
  return this.request(cb, format('wallet/%s/transaction', opts.wallet), 'GET', params);
};


/**
 * @param opts            Information about the wallet being queried.
 * @param opts.wallet     Wallet name, string.
 * @param cb              Callback function (err, json)
 */
Client.prototype.listUTXOs = function(opts, cb) {
  return this.request(cb, format('wallet/%s/utxos', opts.wallet), 'GET');
};


/**
 * @param opts            Information about the proposal being created.
 * @param opts.wallet     Wallet name, string.
 * @param opts.dest       Destination address, string.
 * @param opts.amount     Amount in BTC, string/number, e.g. '0.42'
 * @param opts.note       Custom note about this proposal, optional string
 * @param cb              Callback function (err, json)
 */
Client.prototype.startProposal = function(opts, cb) {
  return this.request(cb, 'transaction/proposal/', 'POST', opts);
};


/**
 * @param opts            Information about the sendmany proposal being created.
 * @param opts.wallet     Wallet name, string.
 * @param opts.recipients Recipients, a list of {<address, string> : <amount, string/number}
 * @param opts.note       Custom note about this proposal, optional string
 * @param cb              Callback function (err, json)
 */
Client.prototype.startManyProposal = function(opts, cb) {
  return this.request(cb, 'transaction/proposal/many', 'POST', opts);
};


/**
 * @param proposal        Proposal returned from startProposal or getProposal.
 * @param cb              Callback function (err, json)
 */
Client.prototype.prepareProposal = function(proposal, cb) {
  if (proposal.state !== 'unprepared') {
    return cb(new LocalError('unexpected proposal state', -400), null);
  }

  var xpub = proposal.xpub_list.map(function(p) {
    return new bitcore.HDPublicKey(p);
  });
  var changeAddy = proposal.change_address.address;
  var threshold = proposal.m;
  var ptx = new bitcore.Transaction();
  proposal.utxo.forEach(function(u) {
    var pubs = xpub.map(function(x) {
      return x.derive(u.path).publicKey;
    });
    ptx = ptx.from(u, pubs, threshold);
  });
  if (!proposal.recipients) {
    proposal.recipients = {};
    proposal.recipients[proposal.dest_address] = proposal.satoshis;
  }
  Object.keys(proposal.recipients).forEach(function(addy) {
    ptx = ptx.to(addy, proposal.recipients[addy]);
  });
  if (proposal.fee_rate && proposal.fee_rate < 0.0025) {
    ptx.feePerKb(Math.round(proposal.fee_rate * 1e8));
  }

  var spare = ptx.getFee();
  var split = this.split;
  if (split && spare >= split.min) {
    var reserve = 0.01 * 1e8;
    var splitAmount = Math.floor((spare - reserve) / split.num);
    if (splitAmount >= 0.02 * 1e8) {
      /* Add extra outputs. */
      for (var i = 0; i < split.num; i++) {
        ptx = ptx.to(changeAddy, splitAmount);
      }
    }
  }
  if (!split || ptx.getFee() >= 0.00015 * 1e8) {
    ptx = ptx.change(changeAddy);
  }
  if(!ptx.verify()) {
    return cb(new LocalError('failed to prepare transaction', -500), null);
  }
  var prepared = JSON.stringify(ptx.toObject());

  var opts = {
    'rawtx': prepared,
    'proposal_id': proposal.id,
    'wallet': proposal.wallet
  };
  return this._rawPrepareProposal(opts, cb);
};

Client.prototype._rawPrepareProposal = function(opts, cb) {
  return this.request(cb, 'transaction/proposal/', 'PUT', opts);
};


/**
 * @param opts            Information about the proposal being returned.
 * @param opts.wallet     Wallet name, string.
 * @param opts.id         Proposal id, integer.
 * @param cb              Callback function (err, json)
 */
Client.prototype.getProposal = function(opts, cb) {
  return this.request(cb, format('transaction/%s/proposal/%s', opts.wallet, opts.id), 'GET');
};


/**
 * @param opts            Information about the proposal being canceled.
 * @param opts.wallet     Wallet name, string.
 * @param opts.id         Proposal id, integer.
 * @param cb              Callback function (err, json)
 */
Client.prototype.deleteProposal = function(opts, cb) {
  return this.request(cb, format('transaction/%s/proposal/%s', opts.wallet, opts.id), 'DELETE');
};


/**
 * @param opts            Information about the proposals being returned.
 * @param opts.wallet     Wallet name, string.
 * @param opts.state      Return proposals in this state, string. One of
 *                          one of: "no_signatures", "partial", "canceled", or
 *                          "complete".
 * @param cb              Callback function (err, json)
 */
Client.prototype.getProposalsByState = function(opts, cb) {
  return this.request(cb, format('transaction/%s/proposal/%s', opts.wallet, opts.state), 'GET');
};


/**
 * @param proposal        Proposal with 0 or more signatures.
 * @param xpriv           Private key used to locally sign.
 * @param cb              Callback function (err, json)
 */
Client.prototype.localSign = function(proposal, xpriv, cb) {
  var serializedTx = JSON.parse(proposal.rawtx);
  var utxo = proposal.utxo;
  var tx = new bitcore.Transaction(serializedTx);
  var xpriv = new bitcore.HDPrivateKey(xpriv);
  var xpub = xpriv.hdPublicKey.toString();

  var seenPk = {};
  var pklist = [];
  utxo.forEach(function(u) {
    var pk = xpriv.derive(u.path).privateKey;
    if (seenPk[pk]) {
      return;
    }
    seenPk[pk] = true;
    pklist.push(pk);
  });

  tx = tx.sign(pklist);
  if (!tx.verify()) {
    return cb(new LocalError('failed to correctly sign transaction', -500));
  }

  var opts = {
    rawtx: JSON.stringify(tx.toObject()),
    wallet: proposal.wallet,
    xpub: xpub,
    proposal_id: proposal.id
  };
  return this._rawSignProposal(opts, cb);
};

Client.prototype._rawSignProposal = function(opts, cb) {
  return this.request(cb, 'transaction/proposal/sign', 'POST', opts);
};


/**
 * @param proposal        Proposal in the complete state.
 * @param cb              Callback function (err, json)
 */
Client.prototype.broadcast = function(proposal, cb) {
  var serializedTx = JSON.parse(proposal.rawtx);
  var tx = new bitcore.Transaction(serializedTx);
  if (!tx.isFullySigned()) {
    return cb(new LocalError('Transaction not fully signed yet', -400));
  }

  var opts = {
    raw: tx.serialize(),
    wallet: proposal.wallet,
    proposal_id: proposal.id
  };
  return this._rawBroadcast(opts, cb);
};

Client.prototype._rawBroadcast = function(opts, cb) {
  return this.request(cb, 'transaction/broadcast', 'POST', opts);
};


/**
 * @param opts            Information about the bitcoin transaction being queried.
 * @param opts.wallet     Wallet name, string.
 * @param opts.txid       Transaction id, string.
 * @param cb              Callback function (err, json)
 */
Client.prototype.txinfo = function(opts, cb) {
  return this.request(cb, format('transaction/%s/%s', opts.wallet, opts.txid), 'GET');
};


module.exports = {
  Client: Client,
  generateKey: generateKey,
  decryptKey: decryptKey,
};
