var crypto = require('crypto');
var assert = require('chai').assert;
var mnsig = require('../index');
var record = require('./record');

describe('Client', function() {

  var recorder = record('mnsig');
  before(recorder.before);

  describe('bad server requests', function() {
    it('should fail with ENOTFOUND', function(done) {
      var cli = new mnsig.Client('', {'host': 'https://doesnotexist.mnsig.com'});
      cli.userdata(function(err, json) {
        assert.isNull(json);
        assert.equal(err.code, 'ENOTFOUND');
        assert.equal(err.statusCode, -1);
        done();
      });
    });

    it('should fail with 404 for an instance that does not exist', function(done) {
      var cli = new mnsig.Client('np');
      cli.userdata(function(err, json) {
        assert.isNull(json);
        assert.equal(err.statusCode, 404);
        assert.equal(err.statusMessage, 'Not found');
        done();
      });
    });

    it('should fail with 400 since no token was specified', function(done) {
      var cli = new mnsig.Client('gpg');
      cli.userdata(function(err, json) {
        assert.isNull(json);
        assert.equal(err.statusCode, 400);
        assert.equal(err.statusMessage.token, 'Specify your user token');
        done();
      });
    });

    it('should fail with 401 since the token is invalid', function(done) {
      var cli = new mnsig.Client('gpg', {'token': 'hello'});
      cli.userdata(function(err, json) {
        assert.isNull(json);
        assert.equal(err.statusCode, 401);
        assert.equal(err.statusMessage, 'Invalid token');
        done();
      });
    });
  });


  describe('user access', function() {
    // These tests assume a pre-existing user under a certain state.
    var token;

    it('should be able to log in', function(done) {
      var cli = new mnsig.Client('gpg');
      var pwd = 'qweasdzxczxc';
      var hashedPwd = crypto.createHash('sha256').update(pwd).digest('hex');
      var data = {'username': 'someuser', 'hashedpwd': hashedPwd};
      cli.login(data, function(err, json) {
        assert.isNull(err);
        assert.equal(json.success, true);
        assert.equal(json.welcome, data.username);
        token = json.token;
        done();
      });
    });

    it('should be able to access its data', function(done) {
      var cli = new mnsig.Client('gpg', {'token': token});
      cli.userdata(function(err, json) {
        assert.isNull(err);
        assert.equal(json.success, true);
        assert.equal(json.wallets.length, 3);

        // It is known that the first wallet (2-of-3) in this case is in use.
        var wallet = json.wallets[0];
        assert.equal(wallet.complete, true);
        assert.equal(wallet.configured, true);
        assert.equal(wallet.num_keys, 3);
        assert.equal(wallet.m, 2)
        assert.equal(wallet.n, wallet.num_keys);
        assert.equal(wallet.derivation, 'bip44');
        assert.equal(wallet.type, 'testnet');
        assert.equal(wallet.balance.confirmed, 0.77);
        assert.equal(wallet.balance.total, 0.77);
        assert.equal(wallet.name, 'my first wallet');
        assert.equal(wallet.pending_proposals.length, 0);
        assert.equal(wallet.users.length, 2);
        assert.equal(wallet.users[0].username, 'someuser');
        assert.equal(wallet.users[1].username, 'otheruser');
        assert.equal(wallet.users[1].permissions[0], 'derive');
        assert.equal(wallet.api_tokens.length, 1);
        assert.equal(wallet.api_tokens[0].max_txamount, 1100000000);
        assert.equal(wallet.api_tokens[0].label, 'remote1');
        assert.equal(wallet.api_tokens[0].last_request_at, null);
        assert.deepEqual(wallet.last_address,
                         {path: 'm/1/18',
                          address: '2NCh8qeMFvP5dsG4sbdqXQ8FFkMgtrTQmoH'});

        // The second wallet (1-of-2) is not ready.
        var wallet = json.wallets[1];
        assert.equal(wallet.complete, false);
        assert.equal(wallet.configured, false);
        assert.equal(wallet.num_keys, 1);
        assert.equal(wallet.m, 1);
        assert.equal(wallet.n, 2);
        assert.equal(wallet.derivation, 'bip44');
        assert.equal(wallet.type, 'testnet');
        assert.equal(wallet.balance, null);
        assert.equal(wallet.name, 'another one');
        assert.equal(wallet.pending_proposals.length, 0);
        assert.equal(wallet.users.length, 2);
        assert.deepEqual(wallet.users[0],
                         {username: 'someuser', permissions: ['admin']});
        assert.equal(wallet.api_tokens.length, 0);
        assert.deepEqual(wallet.last_address, {});
        assert.deepEqual(wallet.last_tx, {});

        done();
      });
    });

    it('should be able to see its completed transactions', function(done) {
      var cli = new mnsig.Client('gpg', {'token': token});
      cli.txlist({wallet: 'my first wallet'}, function(err, json) {
        assert.isNull(err);
        assert.equal(json.success, true);
        assert.equal(json.filtered_count, 1);
        assert.equal(json.total_count, 1);
        assert.equal(json.data.length, 1);
        assert.equal(json.data[0].confirmed, true);
        assert.equal(json.data[0].amount, 77000000);
        done();
      });
    });

    it('should be able to create and cancel a proposal', function(done) {
      var cli = new mnsig.Client('gpg', {'token': token, 'nosplit': true});
      var data = {
        'wallet': 'my first wallet',
        'dest': '2NASwd1heRe2Wd6JJdeKHBtEVL8qGVSc4SJ',
        'amount': '0.01',
        'note': 'nothing to see here'
      };
      //cli.deleteProposal({id: 15, wallet: data.wallet}, function(err, json) {console.log(err, json); done()});
      //return;
      cli.startProposal(data, function(err, json) {
        assert.isNull(err);
        assert.equal(json.success, true);
        assert.equal(json.state, 'unprepared');
        var ddata = {'id': json.id, 'wallet': json.wallet};

        cli.prepareProposal(json, function(err, json) {
          assert.isNull(err);
          assert.equal(json.success, true);
          assert.equal(json.state, 'no_signatures');
          assert.equal(json.updated, true);

          cli.deleteProposal(ddata, function(err, json) {
            assert.isNull(err);
            assert.equal(json.success, true);
            assert.equal(json.state, 'canceled');
            assert.equal(json.id, ddata.id);
            done();
          });
        });
      });
    });

    it('should be able to log out', function(done) {
      var cli = new mnsig.Client('gpg', {'token': token});
      cli.logout(function(err, json) {
        assert.isNull(err);
        // Token is no longer valid.
        assert.equal(json.success, true);
        done();
      });
    });

    it('should not be able to get data with an expired token', function(done) {
      var cli = new mnsig.Client('gpg', {'token': token});
      cli.userdata(function(err, json) {
        assert.isNull(json);
        assert.equal(err.statusCode, 401);
        assert.equal(err.statusMessage, 'Invalid token');
        done();
      });
    });

  });

  after(recorder.after);
});
